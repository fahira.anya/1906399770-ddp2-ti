import java.util.Scanner; // import Scanner class

public class TP1 {
    public static void main(String[] args) {
        // TODO: Kerjakan Soal 1 disini!
        System.out.println("Selamat Datang di DDP2");

        Scanner input = new Scanner(System.in); // cukup sekali aja gapapa

        System.out.print("Masukkan kata: ");
        String kata = input.next();

        System.out.print("Masukkan angka: ");
        int angka = input.nextInt();

        if (Math.pow(angka,2) == 4) 
            System.out.println("Angka bernilai 2");
        else if (angka % 2 == 0)
            System.out.println("Angka genap");
        else
            System.out.println("Bukan genap dan bukan 2");

        
        int length = kata.length();
        if (length < 5 ) {
            System.out.println("Kata yang anda masukkan adalah" + kata);
            System.out.println("Panjang katanya kurang dari 5");
        }
        else if (length > 22)
            System.out.println("Kata yang anda masukkan sangat panjang");
        else
            System.out.println("Kata yang anda masukkan biasa saja");
        
    }
}
