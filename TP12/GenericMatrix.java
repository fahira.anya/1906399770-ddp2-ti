public abstract class GenericMatrix<E extends Number> {
    protected abstract E add(E e1, E e2);
    protected abstract E multiply(E e1, E e2);
    protected abstract E divide(E e1, E e2);
    protected abstract E zero();
    protected abstract E subtract(E e1, E e2);
    protected abstract E negOne();
    protected abstract E posOne();

    public E[][] addMatrix(E[][] m1, E[][] m2) {
        if ((m1.length != m2.length) || (m1[0].length != m2[0].length)) {
            throw new RuntimeException("Ukuran matriks tidak sama");
        }
        E[][] result = (E[][])new Number[m1.length][m1[0].length];

        for (int i = 0; i < result.length; i++)
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = add(m1[i][j], m2[i][j]);
            }
        return result;
    }
    public E[][] multiplyMatrix(E[][] m1, E[][] m2) {
        // Check bounds
        if (m1[0].length != m2.length) {
          throw new RuntimeException("Ukuran matriks tidak sesuai");
        }
    
        // Create result matrix
        E[][] result = (E[][])new Number[m1.length][m2[0].length];
    
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = zero();
                for (int k = 0; k < m1[0].length; k++) {
                    result[i][j] = add(result[i][j],
                        multiply(m1[i][k], m2[k][j]));
                }
            }
        }
        return result;
    }
    public static void printResult(Number[][] m1, Number[][] m2, Number[][] m3, char op) {
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[0].length; j++) {
                System.out.print(" " + m1[i][j]);
                if (i == m1.length /2)
                    System.out.print(" " + op + " ");
                else 
                    System.out.print("    ");

                for (int k = 0; k < m2.length; k++) {
                    System.out.print(" " + m2[i][k]);
                }
                if (i == m1.length / 2)
                    System.out.print("  =  ");
                else
                    System.out.print("     ");
                
                for (int l = 0; l < m3.length; l++) {
                    System.out.print(m3[i][l]);
                }
                System.out.println();
            }
        }
    }
    // SOAL 3
    public boolean sameDimension(E[][] m1, E[][] m2) {
        boolean isSame = true;
        if (m1.length != m2.length || m1[0].length != m2[0].length) {
            isSame = false;
        }
        return isSame;
    }
    public String getSameDimension(E[][] m1, E[][] m2) {
        String result = "Dimensi kedua matriks tidak sama";
        if (sameDimension(m1, m2)) {
            result = "Dimensi kedua matriks sama";
        }
        return result;
    }
    public boolean matriksBujurSangkar(E[][] m) {
        boolean isBujurSangkar = true;
        if (m.length != m[0].length) {
            isBujurSangkar = false;
        }
        return isBujurSangkar;
    }
    public String getBujurSangkar(E[][] m) {
        String result = "Bukan matriks bujur sangkar";
        if (matriksBujurSangkar(m)) {
            result = "Matriks bujur sangkar";
        }
        return result;
    }
    public E[] diagonalPrimer(E[][] m) {
        E[] diagonal = (E[]) new Number[m.length];
        for (int i = 0; i < m.length ; i++) {
            for (int j = 0; j <m[0].length; j++) {
                if (i == j) {
                    diagonal[i] = m[i][j];
                }
            }
        }
        return diagonal;
    }

    public E[][] segitigaAtas(E[][] m) {
        E[][] result = (E[][]) new Number[m.length][m[0].length];
        if (matriksBujurSangkar(m) == true) {
            for (int i = 0; i < m.length; i++) {
                for (int j= 0; j<m.length; j++) {
                    if (i < j || i == j)
                        result[i][j] = m[i][j];
                    else 
                        result[i][j] = zero();
                }
            }
        }
        return result;
    }

    public E detMatrix(E[][] m) {
        if (m.length != 3 && m[0].length != 3) {
            System.out.println("Bukan matriks 3x3");
        }
        E det, x, y, z;
        
        x = subtract(multiply(m[1][1], m[2][2]), multiply(m[1][2], m[2][1]));
        x = multiply(m[0][0], x);

        y = subtract(multiply(m[1][0], m[2][2]), multiply(m[2][0], m[1][2]));
        y = multiply(m[0][1], y);

        z = subtract(multiply(m[1][0], m[2][1]), multiply(m[1][1], m[2][0]));
        z = multiply(m[0][2], z);

        det = subtract(x, y);
        det = add(det, z);

        return det;
    }

    public E[][] transposeMatrix(E[][] m) {
        E[][] result = (E[][]) new Number[m.length][m[0].length];
        for(int i = 0; i < m.length; i++) {
            for(int j = 0; j<m[0].length; j++){
                result[i][j] = m[j][i];
            }
        }
        return result;
    }
    
    public E[][] adjMatrix(E[][] m) {
        E[][] temp = transposeMatrix(m);
        E[][] result = (E[][]) new Number[m.length][m[0].length];
        result[0][0] = multiply(posOne(),subtract(multiply(temp[2][2], temp[1][1]), multiply(temp[1][2], temp[2][1])));
        result[0][1] = multiply(negOne(),subtract(multiply(temp[1][0], temp[2][2]), multiply(temp[1][2], temp[2][0])));
        result[0][2] = multiply(posOne(),subtract(multiply(temp[1][0], temp[2][1]), multiply(temp[1][1], temp[2][0])));

        result[1][0] = multiply(negOne(),subtract(multiply(temp[0][1], temp[2][2]), multiply(temp[2][1], temp[0][2])));
        result[1][1] = multiply(posOne(),subtract(multiply(temp[0][0], temp[2][2]), multiply(temp[2][0], temp[0][2])));
        result[1][2] = multiply(negOne(),subtract(multiply(temp[1][1], temp[2][1]), multiply(temp[2][0], temp[0][1])));

        result[2][0] = multiply(posOne(),subtract(multiply(temp[0][1], temp[1][2]), multiply(temp[1][1], temp[0][2])));
        result[2][1] = multiply(negOne(),subtract(multiply(temp[0][0], temp[1][2]), multiply(temp[1][0], temp[0][2])));
        result[2][2] = multiply(posOne(),subtract(multiply(temp[0][0], temp[1][1]), multiply(temp[1][0], temp[0][1])));

        return result;
    }

    public E[][] inverseMatrix(E[][] m) {
        E[][] result = (E[][]) new Number[m.length][m[0].length];
        E[][] temp = adjMatrix(m);
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                result[i][j] = divide(temp[i][j], detMatrix(m));
            }
        }
        return result;
    }
}