public class JadwalKuliah {
    private String nama;
    private String jam;
    private String hari;

    public JadwalKuliah(String nama, String jam, String hari){
        this.nama = nama;
        this.jam = jam;
        this.hari = hari;
    }

    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getJam() {
        return jam;
    }
    public void setJam(String jam){
        this.jam = jam;
    }
    public String getHari() {
        return hari;
    } 
    public void setHari(String hari) {
        this.hari = hari;
    }
    public int getIndexHari() {
        int harinya = 0;
        if (getHari().equalsIgnoreCase("senin"))
            harinya = 0;
        else if (getHari().equalsIgnoreCase("selasa"))
            harinya = 1;
        else if (getHari().equalsIgnoreCase("rabu"))
            harinya = 2;
        else if (getHari().equalsIgnoreCase("kamis"))
            harinya = 3;
        else if (getHari().equalsIgnoreCase("jumat"))
            harinya = 4;
        else if (getHari().equalsIgnoreCase("sabtu"))
            harinya = 5;
        return harinya;
    }
    public int getIndexJam() {
        int jamnya =0;
        if (getJam().equalsIgnoreCase("8.00"))
            jamnya = 0;
        else if (getJam().equalsIgnoreCase("9.00"))
            jamnya = 1;
        else if (getJam().equalsIgnoreCase("10.00"))
            jamnya = 2;
        else if (getJam().equalsIgnoreCase("11.00"))
            jamnya = 3;
        else if (getJam().equalsIgnoreCase("13.00"))
            jamnya = 4;
        else if (getJam().equalsIgnoreCase("14.00"))
            jamnya = 5;
        else if (getJam().equalsIgnoreCase("15.00"))
            jamnya = 6;
        else if (getJam().equalsIgnoreCase("16.00"))
            jamnya = 7;
        else if (getJam().equalsIgnoreCase("17.00"))
            jamnya = 8;
        return jamnya;
    }
    
}