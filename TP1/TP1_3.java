import java.util.Scanner;

public class TP1_3 {
    public static void main(String[] args) {
        System.out.println("Selamat Datang di Bank Dedepedua!");

        Scanner input = new Scanner(System.in);

        System.out.print("Apa tipe kartu Anda? ");
        String kartu = input.next();

        System.out.print("Sejak tahun berapa Anda menjadi member? ");
        int tahunInput = input.nextInt();
        double hargaBarang = 200000;
        int tahunSkrg = 2020;
        int persen;

        switch (kartu.toLowerCase()) {
            case "gold": persen = 30;
                break;
            case "silver": persen = 15;
                break;
            case "bronze": persen = 5;
                break;
            default : persen = 0;
        }

        if (tahunSkrg - tahunInput > 3)
            persen += 5;
        
        double hargaAkhir = hargaBarang * (100 - persen) / 100;
        System.out.println("Dapat diskon sebanyak " + persen + " persen.");
        System.out.println("Harga barang menjadi Rp" + hargaAkhir);
        
    }
}
