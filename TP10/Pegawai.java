public class Pegawai extends Manusia {
    private String levelKeahlian;
    // constructor
    public Pegawai(String nama, int uang, String levelKeahlian) {
        super(nama, uang);
        this.levelKeahlian = levelKeahlian;
    }
    // setter getter
    public String getLevelKeahlian() {
        return levelKeahlian;
    }
    public void setLevelKeahlian(String levelKeahlian) {
        this.levelKeahlian = levelKeahlian;
    }
    
    @Override
    public String bicara() {
        return "Halo nama saya " + getNama() + ". Uang saya adalah " + getUang() + ", dan level keahlian saya adalah " + getLevelKeahlian() +".";
    }

    public String bekerja(){
        return getNama() + " bekerja di kedai VoidMain.";
    }
    
    public String toString() {
        return bicara();
    }
}
