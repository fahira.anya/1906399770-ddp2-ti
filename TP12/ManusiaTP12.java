public class ManusiaTP12 {
    private String nama;

    public ManusiaTP12(String nama) {
        this.nama = nama;
    }
    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String toString() {
        return "Manusia bernama " + getNama();
    }
}