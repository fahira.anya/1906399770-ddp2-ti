import java.util.*;

public class Kardus<T extends Barang>{
    private List<T> listBarang = new ArrayList<>();

    public String rekap() {
        int n = 0; // elektronik
        int m = 0; // pakaian
        String x = ""; // type
        
        for (T barang : listBarang) {
            try {
                Elektronik elektronik = (Elektronik) barang;
                n++;
            } catch (Exception e) {
                m++;
            }
        }
        if (n == 0) {
            x = "Pakaian";
        }
        else if (m == 0) {
            x = "Elektronik";
        }
        else if (n != 0 && m != 0) {
            x = "Campuran";
        }

        return "Kardus " + x + ": Terdapat " + n + " barang elektronik dan " + m + " pakaian";
    }
    
    public List<T> getKardus() {
        return listBarang;
    }

    public double getTotalValue() {
        double total = 0.0;
        for (T barang : listBarang) {
            total += barang.getValue();
        }
        return total;
    }

    public void tambahBarang(T barang) {
        listBarang.add(barang);
    }
    
}
