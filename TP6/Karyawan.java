public class Karyawan {
    private String nama;
    private int umur;
    private int lamaBekerja;
    private double gaji;

    //TODO
    public Karyawan(String nama, int umur, int lamaBekerja) {
        this.nama = nama;
        this.umur = umur;
        this.lamaBekerja = lamaBekerja;
        gaji = 100;
        setGaji();
    }

    //TODO
    public void setGaji() {
        int n = lamaBekerja / 3;
        for (int i = 0; i < n; i++) {
            gaji += gaji * 0.05;
        }
        if (umur > 40)
            gaji += 10;
    }
    public double getGaji(){
        return gaji;
    } 

    //Buatlah method setter getter yang diperlukan
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }
    
    public int getlamaBekerja() {
        return lamaBekerja;
    }

    public void setlamaBekerja(int lamaBekerja) {
        this.lamaBekerja = lamaBekerja;
    }
}

