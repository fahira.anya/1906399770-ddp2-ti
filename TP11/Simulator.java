import java.util.*;

public class Simulator {
    public static void main(String[] args) {
        Kardus <Elektronik> kardusElektronik = new Kardus<>();
        Kardus <Pakaian> kardusPakaian = new Kardus<>();
        Kardus <Barang> kardusCampuran = new Kardus<>();
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Ada berapa jumlah barang untuk elektronik, pakaian, dan campuran? ");
        int e = sc.nextInt();
        int p = sc.nextInt();
        int c = sc.nextInt();
        
        // elektronik
        ArrayList<String> jenisElektronik = new ArrayList<>();
        jenisElektronik.add("hp");
        jenisElektronik.add("laptop");
        jenisElektronik.add("modem");

        ArrayList<String> kondisiElektronik = new ArrayList<>();
        kondisiElektronik.add("buruk");
        kondisiElektronik.add("menengah");
        kondisiElektronik.add("baik");
        kondisiElektronik.add("baru");

        System.out.println("Silahkan masukkan keterangan elektronik (format: jenis <spasi> kondisi) ");
        int i = 0;
        while (i < e) {
            String jenis = sc.next().toLowerCase();
            String kondisi = sc.next().toLowerCase();
            if (jenisElektronik.contains(jenis) && kondisiElektronik.contains(kondisi)) {
                Elektronik elektronik = new Elektronik(jenis, kondisi);
                kardusElektronik.tambahBarang(elektronik);
                i ++;
            } else {
                System.out.println("Maaf, keterangan elektronik salah!");
            }
        }
    
        // pakaian
        ArrayList<Character> ukuranPakaian = new ArrayList<>();
        ukuranPakaian.add('s');
        ukuranPakaian.add('m');
        ukuranPakaian.add('l');

        System.out.println("Silahkan masukkan keterangan pakaian (format: ukuran <spasi> warna)");
        int j = 0;
        while (j < p) {
            char ukuran = sc.next().toLowerCase().charAt(0);
            String warna = sc.next().toLowerCase();
            if (ukuranPakaian.contains(ukuran)) {
                Pakaian pakaian = new Pakaian(ukuran, warna);
                kardusPakaian.tambahBarang(pakaian);
                j++;
            } else {
                System.out.println("Maaf, tidak ada pakaian dengan ukuran tersebut");
            }
            
        }

        // campuran
        System.out.println("Silahkan masukkan keterangan barang campuran (format: tipe <spasi> jenis <spasi> kondisi ATAU tipe <spasi> ukuran <spasi> warna)");
        int k = 0;
        while (k < c) {
            String tipe = sc.next().toLowerCase();
            if (tipe.equals("pakaian")) {
                char ukuran = sc.next().toLowerCase().charAt(0);
                String warna = sc.next().toLowerCase();
                if (ukuranPakaian.contains(ukuran)) {
                    Pakaian pakaian = new Pakaian(ukuran, warna);
                    kardusCampuran.tambahBarang(pakaian);
                    k ++;
                } else {
                    System.out.printf("Maaf, tidak ada pakaian dengan ukuran %s %n", ukuran);
                }
            }
            else if (tipe.equals("elektronik")){
                String jenis = sc.next().toLowerCase();
                String kondisi = sc.next().toLowerCase();

                if (jenisElektronik.contains(jenis) && kondisiElektronik.contains(kondisi)) {
                    Elektronik elektronik = new Elektronik(jenis, kondisi);
                    kardusCampuran.tambahBarang(elektronik);
                    k ++;
                } else {
                    System.out.println("Maaf, keterangan elektronik salah!");
                }
            }
            else {
                System.out.printf("Tidak ada tipe %s!%n", tipe);
            }
 
        }

        String garis = "-----------------------------------";
        
        System.out.println(garis);
        System.out.println("Terima kasih!");
        System.out.printf("Donasi Anda sebesar %.2f DDD%n", (kardusElektronik.getTotalValue() + kardusPakaian.getTotalValue() + kardusCampuran.getTotalValue()));
        System.out.println(garis);
        System.out.println("Rekap donasi untuk tiap kardus:");
        System.out.println(kardusElektronik.rekap()); // rekap
        System.out.println(kardusPakaian.rekap()); // rekap
        System.out.println(kardusCampuran.rekap()); // rekap
        System.out.println(garis);
        System.out.println("Rekap Asuransi Mitra:");
        System.out.println(); // rekap mitra
        
        sc.close();
    }
}
