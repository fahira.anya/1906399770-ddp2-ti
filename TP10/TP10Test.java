import org.junit.Test;
import static org.junit.Assert.*;

public class TP10Test {
    // test method output
    @Test 
    public void testOutputBergerak() {
        Manusia a = new Pegawai("Anya", 100000, "pemula");
        String gerak = a.bergerak();
        assertEquals(gerak, "Anya bergerak dengan cara berjalan.");
    }
    @Test
    public void testOutputBernafas() {
        Manusia a = new Pegawai("Anya", 100000, "pemula");
        String nafas = a.bernafas();
        assertEquals(nafas, "Anya bernafas dengan paru-paru.");
    }
    @Test
    public void testOutputPegawaiBicara() {
        Manusia a = new Pegawai("Anya", 20000, "pemula");
        String bicara = a.bicara();
        assertEquals(bicara, "Halo nama saya Anya. Uang saya adalah 20000, dan level keahlian saya adalah pemula.");
    }
    @Test
    public void testOutputPelangganBicara() {
        Manusia a = new Pelanggan("Bimo",30000);
        String bicara = a.bicara();
        assertEquals(bicara, "Halo nama saya Bimo. Uang saya adalah 30000.");
    }
    @Test
    public void testOutputPegawaiBekerja() {
        Manusia a = new Pegawai("Bimo",30000, "master");
        String kerja = ((Pegawai)a).bekerja();
        assertEquals(kerja, "Bimo bekerja di kedai VoidMain.");
    }
    @Test
    public void testOutputBeli() {
        Manusia a = new Pelanggan("Joko",10000);
        String beli = ((Pelanggan)a).membeli();
        assertEquals(beli, "Joko membeli makanan dan minuman di kedai VoidMain.");
    }
    @Test
    public void testOutputPegawaiSpesialLibur() {
        Manusia a = new PegawaiSpesial("Anya",10000, "master");
        String libur = ((PegawaiSpesial)a).libur();
        assertEquals(libur, "Anya sedang berlibur ke Akihabara.");
    }
    @Test
    public void testOutputIkanBersuara() {
        Hewan a = new Ikan("Betta", "Betta Splendens", false);
        String suara = ((Ikan)a).bersuara();
        assertEquals(suara, "Blub blub blub blub. Blub. (Halo, saya Betta. Saya ikan yang tidak beracun.)");
    }
    @Test
    public void testOutputIkanSpesialBersuara() {
        Hewan a = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", true);
        String suara = ((IkanSpesial)a).bersuara();
        assertEquals(suara, "Blub blub blub blub. Blub. Blub blub blub. (Halo, saya Ikan Terbang Biru. Saya ikan yang beracun. Saya bisa terbang loh.)");
    }
    @Test
    public void testOutputBurungHantuBersuara() {
        Hewan a = new BurungHantu("Burhan", "Burung Hantu");
        String suara = ((BurungHantu)a).bersuara();
        assertEquals(suara, "Hooooh hoooooh. (Halo, saya Burhan. Saya adalah burung hantu.)");
    }
    @Test
    public void testOutputIkanBernafas() {
        Hewan a = new Ikan("Betta", "Betta Splendens", false);
        String nafas = ((Ikan)a).bernafas();
        assertEquals(nafas, "Betta bernafas dengan insang.");
    }
    @Test
    public void testOutputBurungHantuBernafas() {
        Hewan a = new BurungHantu("Burhan", "Burung Hantu");
        String nafas = ((BurungHantu)a).bernafas();
        assertEquals(nafas, "Burhan bernafas dengan paru-paru.");
    }
    @Test
    public void testOutputIkanSpesialTerbang() {
        Hewan a = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", true);
        String terbang = ((IkanSpesial)a).terbang();
        assertEquals(terbang, "Fwoosh! Plup.");
    }

    // test constructor
    @Test
    public void testPegawaiAttributes() {
        Manusia a = new Pegawai("Anya", 100000, "master");
        assertEquals(a.getNama(), "Anya");
        assertEquals(a.getUang(), 100000);
        assertEquals(((Pegawai)a).getLevelKeahlian(), "master");
    }
    @Test
    public void testPelangganAttributes() {
        Manusia a = new Pelanggan("Bimo",100000);
        assertEquals(a.getNama(), "Bimo");
        assertEquals(a.getUang(), 100000);
    }
    @Test
    public void testIkanAttributes() {
        Hewan a = new Ikan("Betta", "Betta Splendens", false);
        assertEquals(a.getNama(), "Betta");
        assertEquals(a.getSpesies(), "Betta Splendens");
        assertEquals(((Ikan)a).getIsBeracun(), false);
    }
    @Test
    public void testBurungHantuAttributes() {
        Hewan a = new BurungHantu("Burhan", "Burung Hantu");
        assertEquals(a.getNama(), "Burhan");
        assertEquals(a.getSpesies(), "Burung Hantu");
    }
    
    // test subclass
    
    @Test
    public void testSubclassPegawai() {
        assertTrue(Manusia.class.isAssignableFrom(Pegawai.class));

    }
    @Test
    public void testSubclassPegawaiSpesial() {
        assertTrue(Manusia.class.isAssignableFrom(PegawaiSpesial.class));

    }
    @Test
    public void testSubclassPelanggan() {
        assertTrue(Manusia.class.isAssignableFrom(Pelanggan.class));

    }
    @Test
    public void testSubclassIkan() {
        assertTrue(Hewan.class.isAssignableFrom(Ikan.class));
    }
    @Test
    public void testSubclassIkanSpesial() {
        assertTrue(Hewan.class.isAssignableFrom(IkanSpesial.class));
    }
    @Test
    public void testSubclassBurungHantu() {
        assertTrue(Hewan.class.isAssignableFrom(BurungHantu.class));
    }

    // test interface implementation
    @Test
    public void testImplementsMakhlukPegawai() {
        assertTrue(Makhluk.class.isAssignableFrom(Pegawai.class));
    }
    @Test
    public void testImplementsMakhlukPegawaiSpesial() {
        assertTrue(Makhluk.class.isAssignableFrom(PegawaiSpesial.class));
        assertTrue(BisaLibur.class.isAssignableFrom(PegawaiSpesial.class));
    }
    @Test
    public void testImplementsMakhlukPelanggan() {
        assertTrue(Makhluk.class.isAssignableFrom(Pelanggan.class));
    }
    @Test
    public void testImplementsMakhlukIkan() {
        assertTrue(Makhluk.class.isAssignableFrom(Ikan.class));
    }
    @Test
    public void testImplementsMakhlukIkanSpesial() {
        assertTrue(Makhluk.class.isAssignableFrom(IkanSpesial.class));
        assertTrue(BisaTerbang.class.isAssignableFrom(IkanSpesial.class));
    }
    @Test
    public void testImplementsMakhlukBurungHantu() {
        assertTrue(Makhluk.class.isAssignableFrom(BurungHantu.class));
    }

}
