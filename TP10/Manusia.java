public abstract class Manusia implements Makhluk {
    private String nama;
    private int uang;

    // constructor
    protected Manusia (String nama, int uang){
        this.nama = nama;
        this.uang = uang;
    }
    
    // setter getter
    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public int getUang() {
        return uang;
    }
    public void setUang(int uang){
        this.uang = uang;
    }

    @Override
    public String bernafas() {
        return getNama() + " bernafas dengan paru-paru.";
    }
    @Override 
    public String bergerak() {
        return getNama() + " bergerak dengan cara berjalan.";
    }

    public abstract String bicara();
}
