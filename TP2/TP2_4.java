import java.util.Scanner;

public class TP2_4 {
    public static void main(String[] args) {
        System.out.println("Masukkan angka");
        Scanner input = new Scanner(System.in);

        System.out.print("ab: ");
        int ab = input.nextInt();
        System.out.print("bc: ");
        int bc = input.nextInt();
        System.out.print("ac: ");
        int ac = input.nextInt();
        int temp;
        String pemimpin = "";

        boolean siku;

        // ubah angka terbesar jadi c
        if (ab > ac && ab > bc) {
            pemimpin = "c";
            temp = ac;
            ac = ab;
            ab = temp;
        }
        if (bc > ac && bc > ab) {
            pemimpin = "a";
            temp = ac;
            ac = bc;
            bc = temp;
        }
        if (ac > bc && ac > ab) {
            pemimpin = "b";
        }


        // pythagoras
        if (Math.pow(ab, 2) + Math.pow(bc, 2) == Math.pow(ac, 2)){
            System.out.println("segitiga siku-siku, mereka penyusup");
            System.out.println(pemimpin + " pemimpin mereka");
        }     
        else
            System.out.println("bukan segitiga siku-siku, mereka bukan penyusup");
            
        input.close();
    }
}
