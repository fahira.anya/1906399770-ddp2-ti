import java.util.Scanner;

public class TP2_3 {
    public static void main(String[] args) {
        System.out.println("Selamat datang di desa Dedepedua");
        Scanner input = new Scanner(System.in);

        System.out.print("Apa hewan kesukaan Kepala Desa? ");
        String hewan = input.nextLine();

        boolean enter = false;

        if (hewan.equals("burung hantu")) {
            System.out.print("Berapa jumlah harimau di desa Dedepedua? ");
            int harimau = input.nextInt();
            if (harimau == 8 || harimau == 10 || harimau == 18) {
                System.out.print("Berapa kecepatan harimau di desa Dedepedua? ");
                double kecepatan = input.nextDouble();
                if ((100 < kecepatan) && (kecepatan < 120)) {
                    enter = true;
                }
            }
        }

        if (enter) {
            System.out.println("Selamat kamu boleh masuk");
        }  
        else {
            System.out.println("Kamu dilarang masuk");
        }

        input.close();
    }
}
