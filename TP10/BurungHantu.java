public class BurungHantu extends Hewan implements BisaTerbang {
    public BurungHantu(String nama, String spesies){
        super(nama, spesies);
    }
    @Override
    public String bersuara() {
        return "Hooooh hoooooh. (Halo, saya " + getNama() +". Saya adalah burung hantu.)";
    }
    @Override
    public String bernafas() {
        return getNama() + " bernafas dengan paru-paru.";
    }
    @Override
    public String bergerak() {
        return getNama() + " bergerak dengan cara terbang.";
    }
    @Override
    public String terbang() {
        return "Hooh hooh";
    }
    public String toString() {
        return bersuara();
    }
}
