public class Pelanggan extends Manusia {
    public Pelanggan (String nama, int uang) {
        super(nama, uang);
    }
    @Override
    public String bicara() {
        return "Halo nama saya " + getNama() + ". Uang saya adalah " + getUang() +".";
    }

    public String membeli() {
        return getNama() + " membeli makanan dan minuman di kedai VoidMain.";
    }

    public String toString() {
        return bicara();
    }
}
