import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;
import java.io.*;

public class TP12Test {
    // SOAL 1
    @Test
    public void testMatkulAttributes() {
        MataKuliah kul1 = new MataKuliah("PPW", "123456789");
        assertEquals("PPW", kul1.getNama());
        assertEquals("123456789", kul1.getKode());
    }
    @Test
    public void testMahasiswaAttributes() {
        Mahasiswa anya = new Mahasiswa("Anya", "1906399770");
        assertEquals(anya.getNama(), "Anya");
        assertEquals(anya.getNpm(), "1906399770");
    }
    @Test
    public void testMatkulTambahMahasiswa() {
        Mahasiswa mhs1 = new Mahasiswa("Burhan", "129500004Y");
        Mahasiswa mhs2 = new Mahasiswa("Bubur", "1299123456");
        MataKuliah kul1 = new MataKuliah("DDP2", "CSGE601021");

        kul1.tambahMhs(mhs1);
        kul1.tambahMhs(mhs2);

        Mahasiswa[] daftarMhs = kul1.getDaftarMhs();
        List<Mahasiswa> arr = Arrays.asList(daftarMhs);

        assertTrue(arr.contains(mhs1));
        assertTrue(arr.contains(mhs2));
    }
    @Test
    public void testMatkulDropMahasiswa() {
        Mahasiswa mhs1 = new Mahasiswa("Anya", "1906399770");
        Mahasiswa mhs2 = new Mahasiswa("Bubur", "1234567890");
        MataKuliah kul = new MataKuliah("abc", "abcd1234");

        kul.tambahMhs(mhs1);
        kul.tambahMhs(mhs2);
        kul.dropMhs(mhs1);
        
        Mahasiswa[] daftarMhs = kul.getDaftarMhs();
        List<Mahasiswa> arr = Arrays.asList(daftarMhs);

        assertFalse(arr.contains(mhs1));
        assertTrue(arr.contains(mhs2));
    }
    @Test
    public void testMahasiswaTambahMatkul() {
        MataKuliah kul1 = new MataKuliah("AdBis", "abc9876543");
        MataKuliah kul2 = new MataKuliah("PPM", "def746352");
        Mahasiswa mhs = new Mahasiswa("Burhan", "1223344556");

        mhs.tambahMatkul(kul1);
        mhs.tambahMatkul(kul2);

        MataKuliah[] daftarKuliah = mhs.getDaftarMatkul();
        List<MataKuliah> arr = Arrays.asList(daftarKuliah);

        assertTrue(arr.contains(kul1));
        assertTrue(arr.contains(kul2));
    }
    @Test
    public void testMahasiswaDropMatkul() {
        MataKuliah kul1 = new MataKuliah("AdBis", "abc9876543");
        MataKuliah kul2 = new MataKuliah("PPM", "def746352");
        Mahasiswa mhs = new Mahasiswa("Burhan", "1223344556");

        mhs.tambahMatkul(kul1);
        mhs.tambahMatkul(kul2);
        mhs.dropMatkul(kul2);

        MataKuliah[] daftarKuliah = mhs.getDaftarMatkul();
        List<MataKuliah> arr = Arrays.asList(daftarKuliah);

        assertTrue(arr.contains(kul1));
        assertFalse(arr.contains(kul2));
    }

    // SOAL 2
    @Test
    public void testDosenAttributes() {
        Dosen dsn = new Dosen("Bubur", "9876543210");
        assertEquals("Bubur", dsn.getNama());
        assertEquals("9876543210", dsn.getNip());
    }
    @Test
    public void testDosenAssignMatkul() {
        Dosen dsn = new Dosen("Bubur", "876543123");
        MataKuliah kul1 = new MataKuliah("ddp", "csge1233456");
        MataKuliah kul2 = new MataKuliah("sda", "csge1234567");

        dsn.assignMatkul(kul1);
        dsn.assignMatkul(kul2);

        MataKuliah[] daftarKuliah = dsn.getDaftarMatkul();
        List<MataKuliah> arr = Arrays.asList(daftarKuliah);

        assertTrue(arr.contains(kul1));
        assertTrue(arr.contains(kul2));
    }
    @Test
    public void testMatkulAssignDosen() {
        Dosen dsn1 = new Dosen("Bubur", "876543123");
        Dosen dsn2 = new Dosen("Burhan", "9876543");
        MataKuliah kul = new MataKuliah("ddp", "csge1233456");

        kul.assignDosen(dsn1);
        kul.assignDosen(dsn2);

        Dosen[] daftarDosen = kul.getDaftarDosen();
        List<Dosen> arr = Arrays.asList(daftarDosen);

        assertTrue(arr.contains(dsn1));
        assertTrue(arr.contains(dsn2));
    }
    @Test
    public void testFileIsNotExist() {
        MataKuliah kul = new MataKuliah("PPW", "CSGE123456");
        Dosen dsn = new Dosen("Bubur", "876543123");
        Mahasiswa mhs = new Mahasiswa("Bubur", "1234567890");

        kul.assignDosen(dsn);
        kul.tambahMhs(mhs);
        assertFalse(kul.readFile("CSGE123456.txt"));
    }
    @Test
    public void testFileIsExist(){
        MataKuliah kul = new MataKuliah("DDP2", "CSGE601021");
        Mahasiswa mhs1= new Mahasiswa("Bubur", "1234567890");
        Mahasiswa mhs2 = new Mahasiswa("Anya", "0987654323");
        Mahasiswa mhs3 = new Mahasiswa("Burhan", "4567890987");

        kul.tambahMhs(mhs1);
        kul.tambahMhs(mhs2);
        kul.tambahMhs(mhs3);
        kul.writeFile();
        assertTrue(kul.readFile("CSGE601021.txt"));
    }

    // SOAL 3
    @Test
    public void testDimensiSama() {
        Integer[][] m1 = {{1,2,3},{3,4,5},{6,7,8}}; 
        Integer[][] m2 = {{6,7,8},{3,4,5},{1,2,3}};
        IntegerMatrix im = new IntegerMatrix();
        assertTrue(im.sameDimension(m1, m2));
    }

    @Test
    public void testDimensiBeda() {
        Integer[][] m1 = {{6,7,8},{3,4,5},{1,2,3}};
        Integer[][] m2 = {{6,7,8},{3,4,5}};
        IntegerMatrix im = new IntegerMatrix();
        assertFalse(im.sameDimension(m1, m2));
    }
    @Test
    public void testMatriksBujurSangkar() {
        Integer[][] m = {{1,2,3},{3,4,5},{6,7,8}}; 
        IntegerMatrix im = new IntegerMatrix();
        assertTrue(im.matriksBujurSangkar(m));
    }
    @Test
    public void testBukanMatriksBujurSangkar() {
        Integer[][] m = {{1,2,3},{3,4,5}}; 
        IntegerMatrix im = new IntegerMatrix();
        assertFalse(im.matriksBujurSangkar(m));
    }
    @Test
    public void testDiagonalPrimer() {
        Integer[][] m = {{1,2,3},{3,4,5},{6,7,8}}; 
        IntegerMatrix im = new IntegerMatrix();
        Integer[] primer = {1,4,8};
        assertArrayEquals(im.diagonalPrimer(m), primer);
    }
    @Test
    public void testSegitigaAtas() {
        Integer[][] m = {{1,2,3},{3,4,5},{6,7,8}}; 
        IntegerMatrix im = new IntegerMatrix();
        Integer[][] segitiga = {{1,2,3}, {0,4,5},{0,0,8}};
        assertArrayEquals(im.segitigaAtas(m), segitiga);
    }
    @Test
    public void testDeterminan() {
        Integer[][] m = {{1,2,3}, {0,1,4},{5,6,0}}; 
        IntegerMatrix im = new IntegerMatrix();
        assertEquals(im.detMatrix(m),(Integer)1);
    }
    @Test
    public void testTranspose() {
        Integer[][] m = {{1,2,3}, {0,1,4},{5,6,0}}; 
        IntegerMatrix im = new IntegerMatrix();
        Integer[][] transpose = {{1,0,5}, {2,1,6},{3,4,0}}; 
        assertArrayEquals(im.transposeMatrix(m),transpose);
    }
    @Test
    public void testAdjoint() {
        Integer[][] m = {{1,2,3}, {0,1,4},{5,6,0}}; 
        IntegerMatrix im = new IntegerMatrix();
        Integer[][] adj = {{-24,18,5}, {20,-15,-4},{-5,4,1}}; 
        assertArrayEquals(im.adjMatrix(m),adj);
    }
    @Test
    public void testInverse() {
        Integer[][] m = {{1,2,3}, {0,1,4},{5,6,0}}; 
        IntegerMatrix im = new IntegerMatrix();
        Integer[][] inv = {{-24,18,5}, {20,-15,-4},{-5,4,1}}; 
        assertArrayEquals(im.inverseMatrix(m),inv);
    }

}
