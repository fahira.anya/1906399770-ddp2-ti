public class IkanSpesial extends Ikan implements BisaTerbang{
    public IkanSpesial(String nama, String spesies, boolean isBeracun) {
        super(nama, spesies, isBeracun);
    }
    @Override
    public String bersuara() {
        return "Blub blub blub blub. Blub. Blub blub blub. (Halo, saya " + getNama() + ". Saya ikan yang " + getStringBeracun() + ". Saya bisa terbang loh.)";
    }

    @Override
    public String terbang() {
        return "Fwoosh! Plup.";
    }
    
    public String toString() {
        return bersuara();
    }
}
