import java.util.*;
import java.io.*;

public class MataKuliah {
    private String nama;
    private String kode;
    private Mahasiswa[] daftarMhs;
    private Dosen[] daftarDosen;

    public MataKuliah(String nama, String kode) {
        this.nama = nama;
        this.kode = kode;
        this.daftarMhs = new Mahasiswa[60];
        this.daftarDosen = new Dosen[4];
    }
    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getKode() {
        return kode;
    }
    public void setKode(String kode){
        this.kode = kode;
    }
    public Mahasiswa[] getDaftarMhs() {
        return daftarMhs;
    }
    public void setDaftarMhs(Mahasiswa[] daftarMhs) {
        this.daftarMhs = daftarMhs;
    }
    public Dosen[] getDaftarDosen() {
        return daftarDosen;
    }
    public void setDaftarDosen(Dosen[] daftarDosen){
        this.daftarDosen = daftarDosen;
    }
    public void tambahMhs(ManusiaTP12 mhs) {
        for (int i = 0; i < daftarMhs.length; i++) {
            if (daftarMhs[i] == null) {
                daftarMhs[i] = (Mahasiswa) mhs;
                break;
            } 
        }
    }
    public void dropMhs(ManusiaTP12 mhs) {
        for (int i = 0; i < daftarMhs.length; i++) {
            if (daftarMhs[i].equals(mhs)){
                daftarMhs[i] = null;
                break;
            }
        }
    }
    public String toString() {
        return getKode() + ", " + getNama();
    }
    public String getKategoriMatkul() {
        String kategori = "";
        if (getKode().toLowerCase().contains("uige"))
            kategori = "Mata Kuliah Wajib Universitas";
        else if (getKode().toLowerCase().contains("uist"))
            kategori = "Mata Kuliah Wajib Rumpun Sains dan Teknologi";
        else if (getKode().toLowerCase().contains("csge"))
            kategori = "Mata Kuliah Wajib Fakultas";
        else if (getKode().toLowerCase().contains("cscm"))
            kategori = "Mata Kuliah Wajib Program Studi Ilmu Komputer";
        else if (getKode().toLowerCase().contains("csim"))
            kategori = "Mata Kuliah Wajib Program Studi Sistem Informasi";
        else if (getKode().toLowerCase().contains("csce"))
            kategori = "Mata Kuliah Peminatan Program Studi Ilmu Komputer";
        else if (getKode().toLowerCase().contains("csie"))
            kategori = "Mata Kuliah Peminatan Program Studi Sistem Informasi";
        return kategori;
    }
    public void assignDosen(ManusiaTP12 dosen) {
        for (int i = 0; i < daftarDosen.length; i++) {
            if (daftarDosen[i] == null) {
                daftarDosen[i] = (Dosen) dosen;
                break;
            } 
        }
    }
    public String getNamaDosen() {
        String dosen = "";
        for (int i = 0; i < daftarDosen.length; i ++){
            if (daftarDosen[i] != null){
                dosen += (i+1) +". "+ daftarDosen[i].getNama() + "\n";
            }
        }
        return dosen;
    }
    public String getNamaMhs() {
        String mhs = "";
        for (int i = 0; i < daftarMhs.length; i ++){
            if (daftarMhs[i] != null){
                mhs += (i+1) +". "+ daftarMhs[i].getNama() + "\n";
            }
        }
        return mhs;
    }
    public void printDetails() {
        System.out.println("Mata kuliah: " + getNama() + " (kategori: " + getKategoriMatkul() + ")");
        System.out.println("Dosen:\n" + getNamaDosen());
        System.out.println("Peserta:\n" + getNamaMhs());
    }

    public boolean readFile(String filename) {
        boolean isExist = true;
        try {
            File matakuliah = new File("/Users/anya/Desktop/pacil/DDP2/tp/TP12/"+filename);
            Scanner read = new Scanner(matakuliah);
            while (read.hasNextLine()) {
                String data = read.nextLine();
                System.out.println(data);
            }
            read.close();
        } catch (FileNotFoundException e) {
            isExist = false;
            System.out.println("File tidak ditemukan");
            e.printStackTrace();
        }
        return isExist;
    }
    public void writeFile() {
        try {
            FileWriter matakuliah = new FileWriter("/Users/anya/Desktop/pacil/DDP2/tp/TP12/"+getKode()+".txt");
            matakuliah.write(toString() + "\n");
            for (int i = 0; i < daftarDosen.length; i++) {
                if (daftarDosen[i] != null)
                    matakuliah.write(daftarDosen[i].toString()+"\n");
            }
            for (int j = 0; j < daftarMhs.length; j++) {
                if (daftarMhs[j] != null)
                    matakuliah.write(daftarMhs[j].toString() +"\n");
            }
            matakuliah.close();
            System.out.println("File terbuat, silahkan cek " + getKode()+".txt");
        } catch (IOException e) {
            System.out.println("Error");
            e.printStackTrace();
        }
    }
}
