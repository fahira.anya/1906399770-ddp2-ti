import java.util.*;

public class Main {
    private static Makhluk cari(ArrayList<Makhluk> arr, String find){
        for (int i = 0;i< arr.size();i++){
            if (arr.get(i).getNama().toLowerCase().replaceAll("\\s","").contains(find)){
                return arr.get(i);
            }
        }
        return null;
    }
    public static void main(String[] args) {
        ArrayList <Makhluk> lisMakhluk = new ArrayList<>();
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        Manusia bujang = new Pegawai("Bujang", 100000, "pemula");
        lisMakhluk.add(yoga);
        lisMakhluk.add(bujang);

        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Hewan burhan = new BurungHantu("Burhan", "Bubo Snadiacus");
        Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);
        lisMakhluk.add(betta);
        lisMakhluk.add(burhan);
        lisMakhluk.add(ikanTerbang);

        Scanner sc = new Scanner(System.in);
        boolean running = true;
        while (running) {
            System.out.print("Masukkan perintah: ");
            String[] perintah = sc.nextLine().split(" ");
            if (perintah.length == 1) {
                if (perintah[0].equalsIgnoreCase("selesai"))
                    running = false;
                else
                    System.out.println("Maaf, perintah tidak ditemukan!");
            }
            else if (perintah[0].equalsIgnoreCase("panggil")){
                Makhluk makhluk = cari(lisMakhluk, perintah[1]);
                if (makhluk == null){
                    System.out.printf("Maaf, %s tidak pernah melewati/mampir di kedai.%n",perintah[1]);
                }
                else {
                    System.out.println(makhluk.toString());
                }
            }
            else {
                Makhluk makhluk = cari(lisMakhluk, perintah[0]);
                if (makhluk == null){
                    System.out.printf("Maaf, tidak ada makhluk bernama %s.%n",perintah[0]);
                }
                else {
                    if (perintah[1].equalsIgnoreCase("bergerak")) {
                        System.out.println(makhluk.bergerak());
                    }
                    else if (perintah[1].equalsIgnoreCase("bekerja")) {
                        try {
                            System.out.println(((Pegawai)makhluk).bekerja());
                        } catch (Exception e) {
                            System.out.printf("Maaf, pelanggan %s tidak bekerja di kedai VoidMain.%n", makhluk.getNama());
                        }
                    }
                    else if (perintah[1].equalsIgnoreCase("bicara")) {
                        System.out.println(((Manusia) makhluk).bicara());
                    }
                    else if (perintah[1].equalsIgnoreCase("bernafas")) {
                        System.out.println(makhluk.bernafas());
                    }
                    else if (perintah[1].equalsIgnoreCase("beli")) {
                        if (makhluk instanceof Pelanggan)
                            System.out.println(((Pelanggan)makhluk).membeli());
                        else if (makhluk instanceof Ikan)
                            System.out.printf("Maaf, ikan %s tidak bisa beli.%n", ((Hewan) makhluk).getSpesies());
                        else 
                            System.out.printf("Maaf, %s tidak bisa beli.%n", makhluk.getNama());
                    }
                    else if (perintah[1].equalsIgnoreCase("libur")) {
                        try {
                            System.out.println(((PegawaiSpesial)makhluk).libur());
                        } catch (Exception e) {
                            System.out.printf("Maaf, %s tidak bisa libur.%n", makhluk.getNama());
                        }
                    }
                    else if (perintah[1].equalsIgnoreCase("bersuara")){
                        System.out.println(((Hewan) makhluk).bersuara());
                    }
                    else if (perintah[1].equalsIgnoreCase("terbang")){
                        if (makhluk instanceof IkanSpesial)
                            System.out.println(((IkanSpesial)makhluk).terbang());
                        else if (makhluk instanceof BurungHantu)
                            System.out.println(((BurungHantu)makhluk).terbang());
                        else if (makhluk instanceof Pelanggan)
                            System.out.printf("Maaf, pelanggan %s tidak bisa terbang.%n", makhluk.getNama());
                        else if (makhluk instanceof Pegawai)
                            System.out.printf("Maaf, pegawai %s tidak bisa terbang.%n", makhluk.getNama());
                        else 
                            System.out.printf("Maaf, %s tidak bisa terbang.%n", ((Hewan) makhluk).getSpesies());
                    }
                    else {
                        if (makhluk instanceof Pegawai)
                            System.out.printf("Maaf, pegawai %s tidak bisa %s.%n", makhluk.getNama(), perintah[1].toLowerCase());
                        else if (makhluk instanceof Pelanggan)
                            System.out.printf("Maaf, pelanggan %s tidak bisa %s.%n", makhluk.getNama(), perintah[1].toLowerCase());
                        else if (makhluk instanceof Ikan)
                            System.out.printf("Maaf, ikan %s tidak bisa %s.%n", ((Ikan)makhluk).getSpesies(), perintah[1].toLowerCase());
                        else if (makhluk instanceof BurungHantu)
                            System.out.printf("Maaf, %s tidak bisa %s.%n", ((BurungHantu)makhluk).getSpesies(), perintah[1].toLowerCase());
                    }
                }
            }
        }
        System.out.println("Sampai jumpa!");
        sc.close();
    }
}
