import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        Karyawan[] listKaryawan = new Karyawan[n];

        for (int i = 0; i < n; i++) {
            String nama = scan.next();
            int umur = scan.nextInt();
            int lamaBekerja = scan.nextInt();
            Karyawan karyawan = new Karyawan(nama, umur, lamaBekerja);
            listKaryawan[i] = karyawan;
        }
        
        
        System.out.printf("Rata-rata gaji karyawan adalah %.2f%n", rerataGaji(listKaryawan));
        System.out.println("Karyawan dengan gaji tertinggi adalah " + gajiTertinggi(listKaryawan));
        System.out.println("Karyawan dengan gaji terendah adalah " + gajiTerendah(listKaryawan));
        scan.close();
    }

    //TODO
    public static double rerataGaji(Karyawan[] listKaryawan) {
        double total = 0;
        for (Karyawan data : listKaryawan) {
            total += data.getGaji();
        }
        return total / listKaryawan.length;
    }
    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTerendah(Karyawan[] listKaryawan) {
        double temp = listKaryawan[0].getGaji();
        String namaTerendah = "";
        for (Karyawan data : listKaryawan) {
            if (data.getGaji() <= temp){
                temp = data.getGaji();
                namaTerendah = data.getNama();
            }
                
        }
        return namaTerendah ;
    }

    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTertinggi(Karyawan[] listKaryawan) {
        double temp = listKaryawan[0].getGaji();
        String namaTertinggi = "";
        for (Karyawan data : listKaryawan) {
            if (data.getGaji() >= temp) {
                temp = data.getGaji();
                namaTertinggi = data.getNama();
            }
                
        }
        return namaTertinggi ;
    }
}
