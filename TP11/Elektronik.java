public class Elektronik extends Barang{
    private String jenis;
    private String kondisi;

    public Elektronik(String jenis, String kondisi) {
        this.jenis = jenis;
        this.kondisi = kondisi;
    }

    public String getJenis() {
        return jenis;
    }
    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
    public String getKondisi() {
        return kondisi;
    }
    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }
    
    public double getHarga() {
        double harga = 0.0;
        if (jenis.equalsIgnoreCase("laptop")) {
            harga =  500.0;
        }
        else if (jenis.equalsIgnoreCase("hp")) {
            harga = 200.0;
        }
        else if (jenis.equalsIgnoreCase("modem")) {
            harga = 100.0;
        }
        return harga;
    }

    @Override
    public double getValue() {
        double val = 0.0;
        if (kondisi.equalsIgnoreCase("buruk"))
            val = getHarga() * 0.25;
        else if (kondisi.equalsIgnoreCase("menengah"))
            val = getHarga() * 0.8;
        else if (kondisi.equalsIgnoreCase("baik"))
            val = getHarga();
        else if (kondisi.equalsIgnoreCase("baru"))
            val = getHarga() * 1.25;
        
        return val;
    }

}
