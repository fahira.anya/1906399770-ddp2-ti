public class PegawaiSpesial extends Pegawai implements BisaLibur {
    public PegawaiSpesial(String nama, int uang, String levelKeahlian) {
        super(nama, uang, levelKeahlian);
    }

    @Override
    public String bicara() {
        return "Halo nama saya " + getNama() + ". Uang saya adalah " + getUang() + ", dan level keahlian saya adalah " + getLevelKeahlian() +". Saya memiliki privilege yaitu bisa libur.";
    }
    @Override
    public String libur() {
        return getNama() + " sedang berlibur ke Akihabara.";
    }
    public String toString() {
        return bicara();
    }
}
