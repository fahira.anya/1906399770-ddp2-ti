public class Ikan extends Hewan{
    private boolean isBeracun;

    public Ikan(String nama, String spesies, boolean isBeracun){
        super(nama, spesies);
        this.isBeracun = isBeracun;
    }

    public boolean getIsBeracun() {
        return isBeracun;
    }

    public String getStringBeracun() {
        if (isBeracun) {
            return "beracun";
        }
        else {
            return "tidak beracun";
        }
    }
    
    @Override
    public String bersuara() {
        return "Blub blub blub blub. Blub. (Halo, saya " + getNama() + ". Saya ikan yang " + getStringBeracun() + ".)";
    }
    @Override
    public String bernafas() {
        return getNama() + " bernafas dengan insang.";
    }
    @Override
    public String bergerak() {
        return getNama() + " bergerak dengan cara berenang.";
    }
    public String toString() {
        return bersuara();
    }
}
