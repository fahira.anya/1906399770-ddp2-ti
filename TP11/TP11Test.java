import org.junit.Test;
import static org.junit.Assert.*;

public class TP11Test {
    @Test
    public void TestElektronikAttributes() {
        Elektronik e = new Elektronik("laptop", "buruk");
        assertEquals(e.getJenis(), "laptop");
        assertEquals(e.getKondisi(), "buruk");
    }
    @Test
    public void TestPakaianAttributes() {
        Pakaian p = new Pakaian('m', "biru");
        assertEquals(p.getUkuran(), 'm');
        assertEquals(p.getWarna(), "biru");
    }
    @Test
    public void TestGetValueElektronik() {
        Elektronik e = new Elektronik("modem","baru");
        assertEquals(e.getValue(), 125.0, 0.0);
    }
    @Test 
    public void TestGetValuePakaian() {
        Pakaian p = new Pakaian('l', "biru");
        assertEquals(p.getValue(), 40.0, 0.0);
    }
    @Test
    public void TestGetTotalValueElektronik() {
        Kardus <Elektronik> k1 = new Kardus<>();
        Elektronik e1 = new Elektronik("hp", "baru");
        Elektronik e2 = new Elektronik("laptop", "buruk");
        k1.tambahBarang(e1);
        k1.tambahBarang(e2);
        assertEquals(k1.getTotalValue(), 375.0, 0.0);
    }
    @Test
    public void TestGetTotalValuePakaian() {
        Kardus <Pakaian> k2 = new Kardus<>();
        Pakaian p1 = new Pakaian('m', "merah");
        Pakaian p2 = new Pakaian('s', "hitam");
        k2.tambahBarang(p1);
        k2.tambahBarang(p2);
        assertEquals(k2.getTotalValue(),65.0, 0.0);
    }
    @Test
    public void TestGetTotaValueCampuran() {
        Kardus <Barang> k3 = new Kardus<>();
        Pakaian p = new Pakaian('l', "putih");
        Elektronik e = new Elektronik("hp", "menengah");
        k3.tambahBarang(p);
        k3.tambahBarang(e);
        assertEquals(k3.getTotalValue(), 200.0, 0.0);
    }
    @Test 
    public void TestMethodTambahBarang() {
        Kardus <Barang> k4 = new Kardus<>();
        Pakaian p1 = new Pakaian('m', "merah");
        Pakaian p2 = new Pakaian('s', "hitam");
        Elektronik e1 = new Elektronik("hp", "baru");
        k4.tambahBarang(p1);
        k4.tambahBarang(p2);
        k4.tambahBarang(e1);
        assertTrue(k4.getKardus().contains(p1));
        assertTrue(k4.getKardus().contains(p2));
        assertTrue(k4.getKardus().contains(e1));
    }
    @Test
    public void TestMethodRekapElektronik() {
        Kardus <Elektronik> k5 = new Kardus<>();
        Elektronik e1 = new Elektronik("modem", "menengah");
        k5.tambahBarang(e1);
        assertEquals(k5.rekap(), "Kardus Elektronik: Terdapat 1 barang elektronik dan 0 pakaian");
    }
    @Test
    public void TestMethodRekapPakaian() {
        Kardus <Pakaian> k6 = new Kardus<>();
        Pakaian p1 = new Pakaian('s', "hitam");
        Pakaian p2 = new Pakaian('m', "putih");
        k6.tambahBarang(p1);
        k6.tambahBarang(p2);
        assertEquals(k6.rekap(), "Kardus Pakaian: Terdapat 0 barang elektronik dan 2 pakaian");
    }
    @Test
    public void TestMethodRekapCampuran(){
        Kardus <Barang> k7 = new Kardus<>();
        Pakaian p1 = new Pakaian('s', "hijau");
        Elektronik e1 = new Elektronik("modem", "buruk");
        Elektronik e2 = new Elektronik("laptop", "menengah");
        k7.tambahBarang(p1);
        k7.tambahBarang(e1);
        k7.tambahBarang(e2);
        assertEquals(k7.rekap(), "Kardus Campuran: Terdapat 2 barang elektronik dan 1 pakaian");
    }
    
}
