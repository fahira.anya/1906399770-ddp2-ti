import java.util.*;

public class Mahasiswa extends ManusiaTP12 {
    private String npm;
    private MataKuliah[] daftarMatkul;
    private JadwalKuliah[][] jadwalKuliah;

    public Mahasiswa(String nama, String npm) {
        super(nama);
        this.npm = npm;
        this.daftarMatkul = new MataKuliah[10];
        this.jadwalKuliah = new JadwalKuliah[9][6];
    }
    public String getNpm() {
        return npm;
    }
    public void setNpm(String npm) {
        this.npm = npm;
    }
    public MataKuliah[] getDaftarMatkul() {
        return daftarMatkul;
    }
    public String printMatkul() {
        String matkul = "";
        for (int i = 0; i < daftarMatkul.length; i ++){
            if (daftarMatkul[i] != null){
                matkul += (i+1) + ". " + daftarMatkul[i].getNama() + "\n";
            }
        }
        return matkul;
    }
    public void setDaftafMatkul(MataKuliah[] daftarMatkul){
        this.daftarMatkul = daftarMatkul;
    }
    public void tambahMatkul(MataKuliah matkul) {
        for (int i = 0; i < daftarMatkul.length; i++) {
            if (daftarMatkul[i] == null) {
                daftarMatkul[i] = matkul;
                break;
            } 
        }
    }
    public void dropMatkul(MataKuliah matkul) {
        for (int i = 0; i < daftarMatkul.length; i++) {
            if (daftarMatkul[i].equals(matkul)){
                daftarMatkul[i] = null;
                break;
            }
        }
    }
    public String toString() {
        return getNpm() + ", " + getNama();
    }


    // soal 4
    public String getStringHari(int hari) {
        String harinya ="";
        if (hari == 0)
            harinya = "Senin";
        else if (hari == 1)
            harinya = "Selasa";
        else if (hari == 2)
            harinya = "Rabu";
        else if (hari == 3)
            harinya = "Kamis";
        else if (hari == 4)
            harinya = "Jumat";
        else if (hari == 5)
            harinya = "Sabtu";
        return harinya;
    }
    public String getStringjam(int jam){
        String jamnya ="";
        if (jam == 0)
            jamnya = "8.00";
        else if (jam == 1)
            jamnya = "9.00";
        else if (jam == 2)
            jamnya = "10.00";
        else if (jam == 3)
            jamnya = "11.00";
        else if (jam == 4)
            jamnya = "13.00";
        else if (jam == 5)
            jamnya = "14.00";
        else if (jam == 6)
            jamnya = "15.00";
        else if (jam == 7)
            jamnya = "16.00";
        else if (jam == 8)
            jamnya = "17.00";
        
        return jamnya;
    }
    public void tambahJadwal(JadwalKuliah jadwal) {
        for (int i = 0; i < jadwalKuliah.length; i++) {
            for (int j = 0; j < jadwalKuliah[0].length; j++) {
                if (i == jadwal.getIndexJam() && j == jadwal.getIndexHari() && jadwalKuliah[i][j] == null) {
                    jadwalKuliah[i][j] = jadwal;
                    break;
                }
            }
        }
    }
    public JadwalKuliah[][] gabungJadwal(Mahasiswa m) {
        JadwalKuliah[][] hasil = m.getJadwalKuliah().clone();
        for (int i = 0; i < hasil.length; i++) {
            for (int j = 0; j < hasil[0].length; j++) {
                if (hasil[i][j] == null)
                    hasil[i][j] = jadwalKuliah[i][j];
            }
        }
        return hasil;
    }
    public void printJadwal(JadwalKuliah[][] jadwal) {
        for (int i = 0; i < jadwal[0].length; i++) {
            System.out.println("Jadwal hari: " + getStringHari(i));
            for (int j = 0; j < jadwal.length; j++) {
                if (jadwal[j][i] == null) 
                    System.out.println(getStringjam(j)+ " - " + "jam kosong");
                else
                    System.out.println(getStringjam(j)+  " - " + jadwal[j][i].getNama());
            }
            System.out.println();
        } 
    }
    public JadwalKuliah[][] getJadwalKuliah() {
        return jadwalKuliah;
    }
}
