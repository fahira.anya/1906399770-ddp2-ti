public class Dosen extends ManusiaTP12 {
    private String nip;
    private MataKuliah[] daftarMatkul;

    public Dosen(String nama, String nip) {
        super(nama);
        this.nip = nip;
        this.daftarMatkul = new MataKuliah[2];
    }
    public String getNip() {
        return nip;
    }
    public void setNip(String nip) {
        this.nip = nip;
    }
    public MataKuliah[] getDaftarMatkul() {
        return daftarMatkul;
    }
    public void assignMatkul(MataKuliah matkul) {
        for (int i = 0; i < daftarMatkul.length; i++) {
            if (daftarMatkul[i] == null) {
                daftarMatkul[i] = matkul;
                break;
            } 
        }
    }
    public String toString() {
        return getNip() + ", " + getNama();
    }
}
